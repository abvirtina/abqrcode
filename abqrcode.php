<?php
/**
 * Plugin Name: AB QR Code PLugin
 * Description: QR code generator with image
 * Version: 1.0
 * Author: AB
 */

require_once("phpqrcode/qrlib.php");

global $post;
$postId=$post->ID;

// Path where the images will be saved
$filepath = 'qrcode.png';

// Image (logo) to be drawn
$logopath = 'http://localhost/qrcode/virtina_logo.png';

// qr code content
//$codeContents = 'http://ourcodeworld.com';
$codeContents = get_permalink( $postId );

// Create the file in the providen path
// Customize how you want
QRcode::png($codeContents,$filepath , QR_ECLEVEL_H, 20);

// Start DRAWING LOGO IN QRCODE

$QR = imagecreatefrompng($filepath);

// START TO DRAW THE IMAGE ON THE QR CODE
$logo = imagecreatefromstring(file_get_contents($logopath));

/**
 *  Fix for the transparent background
 */
imagecolortransparent($logo , imagecolorallocatealpha($logo , 0, 0, 0, 127));
imagealphablending($logo , false);
imagesavealpha($logo , true);

$QR_width = imagesx($QR);
$QR_height = imagesy($QR);

$logo_width = imagesx($logo);
$logo_height = imagesy($logo);

// Scale logo to fit in the QR Code
$logo_qr_width = $QR_width/3;
$scale = $logo_width/$logo_qr_width;
$logo_qr_height = $logo_height/$scale;

imagecopyresampled($QR, $logo, $QR_width/3, $QR_height/3, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);

// Save QR code again, but with logo on it
imagepng($QR,$filepath);

// End DRAWING LOGO IN QR CODE

// Ouput image in the browser
//echo '<img src="'.$filepath.'" />';


add_action( 'admin_menu', 'ab_qr_code_register' );

function ab_qr_code_register()
{
    add_menu_page(
        'AB QR Code Page',     // page title
        'AB QR Code',     // menu title
        'manage_options',   // capability
        'ab-qr-code',     // menu slug
        'ab_qr_code_render' // callback function
    );
}
function ab_qr_code_render()
{
    global $title;

    print '<div class="wrap">';
    print "<h1>$title</h1>";

    $file = plugin_dir_path( __FILE__ ) . "included.html";

    if ( file_exists( $file ) )
        require $file;

    print "<p class='description'>Included from <code>$file</code></p>";

    print '</div>';
}


add_action( 'add_meta_boxes', 'ab_custom_meta_box' );
function ab_custom_meta_box() {
	$post_types = get_post_types();
	foreach ( $post_types as $post_type ) {
		if($post_type == 'pdf-viewer') {
    	    add_meta_box( 'meta_id', 'QR Permalink Generator', 'ab_qr_code_show', $post_type, 'normal', 'high' );
		}
	}    
}

function ab_qr_code_show()
{
	global $post;
    $postId=$post->ID;
    echo '<img src="'.$filepath.'" />';
    echo '<a href="/images/myw3schoolsimage.jpg" download="QRcode">';
}